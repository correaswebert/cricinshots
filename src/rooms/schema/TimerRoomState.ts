import { Schema, MapSchema, type } from "@colyseus/schema";

export class Player extends Schema {
  @type("number")
  x = Math.floor(Math.random() * 400);

  @type("number")
  y = Math.floor(Math.random() * 400);

  @type("number")
  joinTime = Date.now();
}

export class TimerRoomState extends Schema {
  @type({ map: Player })
  players = new MapSchema<Player>();

  createPlayer(sessionId: string) {
    this.players.set(sessionId, new Player());
  }

  removePlayer(sessionId: string) {
    this.players.delete(sessionId);
  }

  movePlayer(sessionId: string, movement: any) {
    if (movement.x) {
      this.players.get(sessionId).x += movement.x * 10;
    } else if (movement.y) {
      this.players.get(sessionId).y += movement.y * 10;
    }
  }

  resetTimer(sessionId: string) {
    this.players.get(sessionId).joinTime = Date.now();
  }
}
