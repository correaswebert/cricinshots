import { Room, Client } from "colyseus";
import { TimerRoomState } from "./schema/TimerRoomState";

export class TimerRoom extends Room<TimerRoomState> {
  maxClients = 10;

  onCreate(options: any) {
    console.log("TimerRoom created!", options);

    this.setState(new TimerRoomState());

    this.onMessage("move", (client, data) => {
      this.state.movePlayer(client.sessionId, data);
    });

    this.onMessage("reset_timer", (client, _data) => {
      this.state.resetTimer(client.sessionId);
    });
  }

  onJoin(client: Client) {
    this.state.createPlayer(client.sessionId);
  }

  onLeave(client: Client) {
    this.state.removePlayer(client.sessionId);
  }

  onDispose() {
    console.log("Dispose TimerRoom");
  }
}
