import Arena from "@colyseus/arena";
import { monitor } from "@colyseus/monitor";
import express from "express";
import path from "path";
import { TimerRoom } from "./rooms/TimerRoom";
import serveStatic from 'serve-static'

export default Arena({
  getId: () => "crininshots",

  initializeGameServer: (gameServer) => {
    gameServer.define("timer_room", TimerRoom).enableRealtimeListing();
  },

  initializeExpress: (app) => {
    app.use(serveStatic('public', { index: ['index.html'] }))

    /**
     * Bind @colyseus/monitor
     * It is recommended to protect this route with a password.
     * Read more: https://docs.colyseus.io/tools/monitor/
     */
    app.use("/colyseus", monitor());
  },

  beforeListen: () => {
    /**
     * Before before gameServer.listen() is called.
     */
  },
});
