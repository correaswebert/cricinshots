# ensure the exact baseimage is used (exemplified below)
FROM node:lts-alpine3.15

# install dumb-init for invoking docker
RUN apk add dumb-init

# optimize for production
ENV NODE_ENV production

WORKDIR /app

# 'node' process owns the files now, not 'root'
COPY --chown=node:node . .

# do not install dev dependencies
RUN npm install --production=true

# make 'node' as the process owner, not 'root'
USER node

# prevent starting process as PID1 by using dumb-init
CMD ["dumb-init", "npm", "run", "start"]
